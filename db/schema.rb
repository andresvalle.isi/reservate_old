# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140930200208) do

  create_table "bands", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "events", force: true do |t|
    t.integer  "event_id"
    t.date     "start_date"
    t.date     "end_date"
    t.time     "start_time"
    t.time     "end_time"
    t.date     "virtual_date"
    t.integer  "status"
    t.integer  "meta_event_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "meta_events", force: true do |t|
    t.integer  "meta_event_id"
    t.datetime "start_dt"
    t.datetime "end_dt"
    t.date     "virtual_date_start"
    t.date     "virtual_date_end"
    t.integer  "room_id"
    t.integer  "musician_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "musicians", force: true do |t|
    t.integer  "dni"
    t.string   "name"
    t.string   "lastname"
    t.string   "phone"
    t.string   "cellphone"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "reservation_types", force: true do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "reservations", force: true do |t|
    t.integer  "room_id"
    t.integer  "reservation_type_id"
    t.integer  "musician_id"
    t.integer  "band_id"
    t.date     "virtual_day"
    t.datetime "since"
    t.datetime "until"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "room_prices", force: true do |t|
    t.integer  "room_id"
    t.integer  "reservation_type_id"
    t.decimal  "amount"
    t.date     "since"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "rooms", force: true do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
