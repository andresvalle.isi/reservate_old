class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.integer :event_id
      t.date :start_date
      t.date :end_date
      t.time :start_time
      t.time :end_time
      t.date :virtual_date
      t.integer :status
      t.integer :meta_event_id
      t.boolean :cancelled
      t.boolean :edited

      t.timestamps
    end
  end
end
