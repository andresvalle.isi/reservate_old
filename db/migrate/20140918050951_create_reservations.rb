class CreateReservations < ActiveRecord::Migration
  def change
    create_table :reservations do |t|
      t.integer :room_id
      t.integer :reservation_type_id
      t.integer :musician_id
      t.integer :band_id
      t.date :virtual_day
      t.datetime :since
      t.datetime :until

      t.timestamps
    end
  end
end
