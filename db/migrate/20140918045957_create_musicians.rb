class CreateMusicians < ActiveRecord::Migration
  def change
    create_table :musicians do |t|
      t.integer :dni
      t.string :name
      t.string :lastname
      t.string :phone
      t.string :cellphone

      t.timestamps
    end
  end
end
