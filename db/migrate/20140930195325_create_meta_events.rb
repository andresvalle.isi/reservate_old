class CreateMetaEvents < ActiveRecord::Migration
  def change
    create_table :meta_events do |t|
      t.integer :meta_event_id
      t.datetime :start_dt
      t.datetime :end_dt
      t.date :virtual_date_start
      t.date :virtual_date_end
      t.integer :room_id
      t.integer :musician_id
      t.boolean :cancelled
      t.boolean :edited

      t.timestamps
    end
  end
end
