class CreateRoomPrices < ActiveRecord::Migration
  def change
    create_table :room_prices do |t|
      t.integer :room_id
      t.integer :reservation_type_id
      t.decimal :amount
      t.date :since

      t.timestamps
    end
  end
end
