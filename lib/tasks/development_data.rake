namespace :development_data do
  desc "Crea 3 salas de ensayo"
  task create_rooms: :environment do
    Room.create( name: 'Sala 1', description: 'Descripcion de la sala 1.');
    Room.create( name: 'Sala 2', description: 'Descripcion de la sala 2.');
    Room.create( name: 'Sala 3', description: 'Descripcion de la sala 3.');
  end

  desc "Crea algunos musicos"
  task create_musicians: :environment do
    Musician.create( dni: 111, name: 'Jimmy', lastname: 'Page' );
    Musician.create( dni: 222, name: 'Robert', lastname: 'Plant' );
    Musician.create( dni: 333, name: 'John Paul', lastname: 'Jones' );
    Musician.create( dni: 444, name: 'John', lastname: 'Bonham' );
    Musician.create( dni: 555, name: 'David', lastname: 'Gilmour' );
    Musician.create( dni: 666, name: 'Nick', lastname: 'Mason' );
  end

  desc "Crea algunas bandas"
  task create_bands: :environment do
    Band.create( name: 'Led Zeppelin' );
    Band.create( name: 'Pink Floyd' );
    Band.create( name: 'Lynyrd Skynyrd' );
  end

  desc "Crea todos los CP para eventos & meta_eventos"
  task create_events: :environment do
    
    date_1 = Date.new(2014,10,1)
    date_2 = Date.new(2014,10,8)
    date_3 = Date.new(2014,10,15)

    tz = "+00:00"

    # no fijo
    # 1   2014-10-01 12:00:00 2014-10-01 13:00:00 2014-10-08  0   N
    # 2   2014-10-08 12:00:00 2014-10-08 13:00:00 2014-10-08  0   N
    # 3   2014-10-15 12:00:00 2014-10-15 13:00:00 2014-10-08  0   N
    MetaEvent.create( start_dt: Time.new(2014,10, 1,12, 0, 0), end_dt: Time.new(2014,10, 1,13, 0, 0), virtual_date_start: date_1, virtual_date_end: date_1, room_id: 1, musician_id: 1 );
    MetaEvent.create( start_dt: Time.new(2014,10, 1,12, 0, 0), end_dt: Time.new(2014,10, 1,13, 0, 0), virtual_date_start: date_2, virtual_date_end: date_2, room_id: 1, musician_id: 1 );
    MetaEvent.create( start_dt: Time.new(2014,10, 1,12, 0, 0), end_dt: Time.new(2014,10, 1,13, 0, 0), virtual_date_start: date_3, virtual_date_end: date_3, room_id: 1, musician_id: 1 );
    
    # no fijo + cancelado
    # 4   2014-10-01 13:00:00 2014-10-01 14:00:00 2014-10-08  0   N
    # 5   2014-10-08 13:00:00 2014-10-08 14:00:00 2014-10-08  0   N
    # 6   2014-10-15 13:00:00 2014-10-15 14:00:00 2014-10-08  0   N
    MetaEvent.create( start_dt: Time.new(2014,10, 1,13, 0, 0), end_dt: Time.new(2014,10, 1,14, 0, 0), virtual_date_start: date_1, virtual_date_end: date_1, room_id: 1, musician_id: 1 );
    MetaEvent.create( start_dt: Time.new(2014,10, 1,13, 0, 0), end_dt: Time.new(2014,10, 1,14, 0, 0), virtual_date_start: date_1, virtual_date_end: date_1, room_id: 1, musician_id: 1 );
    MetaEvent.create( start_dt: Time.new(2014,10, 1,13, 0, 0), end_dt: Time.new(2014,10, 1,14, 0, 0), virtual_date_start: date_1, virtual_date_end: date_1, room_id: 1, musician_id: 1 );

    Event.create( meta_event_id: 4, start_dt: Time.new(2014,10, 1,13, 0, 0), end_dt: Time.new(2014,10, 1,14, 0, 0), virtual_day: date_1, status: 2 );
    Event.create( meta_event_id: 5, start_dt: Time.new(2014,10, 1,13, 0, 0), end_dt: Time.new(2014,10, 1,14, 0, 0), virtual_day: date_1, status: 2 );
    Event.create( meta_event_id: 6, start_dt: Time.new(2014,10, 1,13, 0, 0), end_dt: Time.new(2014,10, 1,14, 0, 0), virtual_day: date_1, status: 2 );

    # fijo + con limite
    # 7   2014-10-01 14:00:00 2014-10-01 15:00:00 2014-10-01  1   2014-10-01
    # 8   2014-10-01 15:00:00 2014-10-01 16:00:00 2014-10-01  1   2014-10-08
    # 9   2014-10-01 16:00:00 2014-10-01 17:00:00 2014-10-01  1   2014-10-15
    # 10  2014-10-08 17:00:00 2014-10-01 18:00:00 2014-10-08  1   2014-10-08
    # 11  2014-10-08 18:00:00 2014-10-08 19:00:00 2014-10-08  1   2014-10-15
    MetaEvent.create( start_dt: Time.new(2014,10, 1,14, 0, 0), end_dt: Time.new(2014,10, 1,15, 0, 0), virtual_date_start: date_1, virtual_date_end: date_1, room_id: 1, musician_id: 1 );
    MetaEvent.create( start_dt: Time.new(2014,10, 1,15, 0, 0), end_dt: Time.new(2014,10, 1,16, 0, 0), virtual_date_start: date_1, virtual_date_end: date_2, room_id: 1, musician_id: 1 );
    MetaEvent.create( start_dt: Time.new(2014,10, 1,16, 0, 0), end_dt: Time.new(2014,10, 1,17, 0, 0), virtual_date_start: date_1, virtual_date_end: date_3, room_id: 1, musician_id: 1 );
    MetaEvent.create( start_dt: Time.new(2014,10, 8,17, 0, 0), end_dt: Time.new(2014,10, 8,18, 0, 0), virtual_date_start: date_2, virtual_date_end: date_2, room_id: 1, musician_id: 1 );
    MetaEvent.create( start_dt: Time.new(2014,10, 8,18, 0, 0), end_dt: Time.new(2014,10, 8,19, 0, 0), virtual_date_start: date_2, virtual_date_end: date_3, room_id: 1, musician_id: 1 );

    # fijo + sin limite
    # 12  2014-10-01 19:00:00 2014-10-01 20:00:00 2014-10-08  1   N
    # 13  2014-10-01 20:00:00 2014-10-01 21:00:00 2014-10-08  1   N
    # 14  2014-10-08 21:00:00 2014-10-08 22:00:00 2014-10-08  1   N
    # 15  2014-10-08 22:00:00 2014-10-08 23:00:00 2014-10-08  1   N
    MetaEvent.create( start_dt: Time.new(2014,10, 1,19, 0, 0), end_dt: Time.new(2014,10, 1,20, 0, 0), virtual_date_start: date_1, virtual_date_end: nil, room_id: 1, musician_id: 1 );
    MetaEvent.create( start_dt: Time.new(2014,10, 1,20, 0, 0), end_dt: Time.new(2014,10, 1,21, 0, 0), virtual_date_start: date_1, virtual_date_end: nil, room_id: 1, musician_id: 1 );
    MetaEvent.create( start_dt: Time.new(2014,10, 8,21, 0, 0), end_dt: Time.new(2014,10, 8,22, 0, 0), virtual_date_start: date_2, virtual_date_end: nil, room_id: 1, musician_id: 1 );
    MetaEvent.create( start_dt: Time.new(2014,10, 8,22, 0, 0), end_dt: Time.new(2014,10, 8,23, 0, 0), virtual_date_start: date_2, virtual_date_end: nil, room_id: 1, musician_id: 1 );

    # fijo + cancelado
    # 15  2014-10-01 08:00:00 2014-10-01 09:00:00 2014-11-01  1   2014-10-08 -> cancelado el 8
    # 16  2014-10-01 09:00:00 2014-10-01 10:00:00 2014-11-01  1   N          -> cancelado el 8
    # 15  2014-10-08 10:00:00 2014-10-08 11:00:00 2014-11-01  1   2014-10-15 -> cancelado el 8
    # 16  2014-10-08 11:00:00 2014-10-08 12:00:00 2014-11-01  1   N          -> cancelado el 8
    MetaEvent.create( start_dt: Time.new(2014,10, 1, 8, 0, 0), end_dt: Time.new(2014,10, 1, 9, 0, 0), virtual_date_start: date_1, virtual_date_end: date_1, room_id: 1, musician_id: 1 );
    MetaEvent.create( start_dt: Time.new(2014,10, 1, 9, 0, 0), end_dt: Time.new(2014,10, 1,10, 0, 0), virtual_date_start: date_1, virtual_date_end: nil,    room_id: 1, musician_id: 1 );
    MetaEvent.create( start_dt: Time.new(2014,10, 8,10, 0, 0), end_dt: Time.new(2014,10, 8,11, 0, 0), virtual_date_start: date_2, virtual_date_end: date_2, room_id: 1, musician_id: 1 );
    MetaEvent.create( start_dt: Time.new(2014,10, 8,11, 0, 0), end_dt: Time.new(2014,10, 8,12, 0, 0), virtual_date_start: date_2, virtual_date_end: nil,    room_id: 1, musician_id: 1 );

    Event.create( meta_event_id: 15, start_dt: Time.new(2014,10, 1, 8, 0, 0), end_dt: Time.new(2014,10, 1, 9, 0, 0), virtual_day: date_2, status: 2 );
    Event.create( meta_event_id: 16, start_dt: Time.new(2014,10, 1, 9, 0, 0), end_dt: Time.new(2014,10, 1,10, 0, 0), virtual_day: date_2, status: 2 );
    Event.create( meta_event_id: 17, start_dt: Time.new(2014,10, 1,10, 0, 0), end_dt: Time.new(2014,10, 1,11, 0, 0), virtual_day: date_2, status: 2 );
    Event.create( meta_event_id: 18, start_dt: Time.new(2014,10, 1,11, 0, 0), end_dt: Time.new(2014,10, 1,12, 0, 0), virtual_day: date_2, status: 2 );

  end



end
