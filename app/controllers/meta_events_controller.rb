class MetaEventsController < ApplicationController
  before_action :set_meta_event, only: [:show, :edit, :update]

  # GET /meta_events
  # GET /meta_events.json
  def index
    @meta_events = MetaEvent.all
  end

  # GET /meta_events/1
  # GET /meta_events/1.json
  def show
  end

  # GET /meta_events/new
  def new
    @meta_event = MetaEvent.new
  end

  # GET /meta_events/1/edit
  def edit
  end

  # POST /meta_events
  # POST /meta_events.json
  def create
    @meta_event = MetaEvent.new(meta_event_params)

    respond_to do |format|
      if @meta_event.save
        format.html { redirect_to @meta_event, notice: 'Meta event was successfully created.' }
        format.json { render :show, status: :created, location: @meta_event }
      else
        format.html { render :new }
        format.json { render json: @meta_event.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /meta_events/1
  # PATCH/PUT /meta_events/1.json
  def update
    respond_to do |format|
      if @meta_event.update(meta_event_params)
        format.html { redirect_to @meta_event, notice: 'Meta event was successfully updated.' }
        format.json { render :show, status: :ok, location: @meta_event }
      else
        format.html { render :edit }
        format.json { render json: @meta_event.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /meta_events/1
  # DELETE /meta_events/1.json
  def destroy
    @meta_event.destroy
    respond_to do |format|
      format.html { redirect_to meta_events_url, notice: 'Meta event was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_meta_event
      @meta_event = MetaEvent.find(params[:id])
      @event = Event.find(params[:event_id]) if params[:event_id]
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def meta_event_params
      params.require(:meta_event).permit(:meta_event_id, :start_dt, :end_dt, :room_id, :musician_id)
    end
end
