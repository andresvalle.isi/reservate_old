class Musician < ActiveRecord::Base
  has_many :reservations
  validates :dni, :name, :lastname, presence: true
  validates_uniqueness_of :dni
end
