class Reservation < ActiveRecord::Base
  has_one :muscian
  has_one :band
  validates :room_id, :musician_id, :virtual_day, :since, :until, presence: true # TODO :reservation_type_id,


  validate :validate_superposition, :validate_dates_chronology

  # devuelve true cuando se superpone con otra
  def validate_superposition

    self_condition = "AND id != #{self.id}" if ( self.id  ) # excluir de la consulta
    other_reservation = Reservation.where( 
      "since < ? AND until > ? #{self_condition} AND room_id = ?", 
      self.until, self.since, self.room_id
    ).first

    room_name = Room.find( self.room_id ).name

    return true if other_reservation.nil?

    self.errors[:base] << "La sala #{room_name} ya ha sido reservada."
    return false
  end

  # devuelve true cuando la fecha desde no es anterior a la fecha hasta
  def validate_dates_chronology
    return true if self.since < self.until

    self.errors[:base] << "Los horarios ingresados no son correctos."
    return false
  end
end
