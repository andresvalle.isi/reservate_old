class RoomPrice < ActiveRecord::Base
  belongs_to :rooms
  validates :room_od, :reservation_type_id, :amount, :since, presence: true
  # validates_uniqueness_of [ :room_id, :since ] # TODO
end
