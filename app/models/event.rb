class Event < ActiveRecord::Base
  has_one :meta_event

  scope :specified_virtual_date, ->(date)       { where('virtual_date = ?', date) }
  scope :specified_dates,        ->(dates)      { where('virtual_date IN (?)', dates) }
  scope :start_time_limit,       ->(start_time) { where('start_time >= ?', start_time) }
  scope :end_date_limit,         ->(end_date)   { where('end_date >= ?', end_date) }
  scope :cancelled,              ->             { where('cancelled IS NULL') }
  scope :not_cancelled,          ->             { where('cancelled IS NOT NULL') }
  scope :specified_room,         ->(room_id)    { where( room_id: room_id) }

  validates :start_date, :end_date, :start_time, :end_time, :virtual_date, :meta_event_id, presence: true
  validate :overrides_event, :date_chronology, :time_chronology

  # devuelve true cuando se superpone con otra
  def overrides_event

    another_events = self.getOverrideEvents

    return true if another_events.nil?

    self.errors[:base] << "Existe al menos otro evento registrado en ese dia/horario."
    return false

  end

  def date_chronology
    return true if self.start_date < self.end_date

    self.errors[:base] << "Los dias ingresados no son correctos."
    return false
  end

  def time_chronology
    return true if self.start_time < self.end_time

    self.errors[:base] << "Los horarios ingresados no son correctos."
    return false
  end

  def self.GetAgendaEvents(date)
    Event.specified_virtual_date( args.virtual_date ).not_cancelled()
  end

  def getOverrideEvents()
    dates = (self.start_date..self.end_date)
    room_id = self.meta_event.room_id
    scope = Event.specified_room( room_id )
                 .specified_dates( dates )
                 .not_cancelled()
                 .start_time_limit( self.start_time )
                 .end_time_limit( self.end_time )

    scope.exclude_event_id( self.event_id ) if self.event_id

    return scope
  end

end
