class MetaEvent < ActiveRecord::Base
  has_one :musician
  has_one :room
  has_many :events
  validates :start_dt, :end_dt, :virtual_date_start, :virtual_date_end, :room_id, :musician_id, presence: true

  validate :date_chronology, :time_chronology
  validate :meta_event_limit , :on => [:create]

  before_save :set_virtual_dates

  def save
    begin
      transaction do
        super!
        self.save_events!
        return self
      end
    rescue ActiveRecord::RecordInvalid => invalid
      # do whatever you wish to warn the user, or log something
      self.errors[:base] << "Ha ocurrido un error al guardar."
      return false
    end
  end

  def save_events!
    (self.start_date..self.end_date).each do |date|
      Event.create!(
        start_date: date, 
        end_date: date, 
        start_time: self.start_time, 
        end_time: self.end_time, 
        virtual_date: date,
        room_id: self.room_id
        meta_event: self
      )
    end
  end

  def update
    begin
      transaction do
        super!
        self.update_events!
        return self
      end
    rescue ActiveRecord::RecordInvalid => invalid
      # do whatever you wish to warn the user, or log something
      self.errors[:base] << "Ha ocurrido un error al guardar."
      return false
    end
  end

  def update_events! 
    # self.events
    # (self.start_date..self.end_date).each do |date|
    #   Event.create!(
    #     start_date: date, 
    #     end_date: date, 
    #     start_time: self.start_time, 
    #     end_time: self.end_time, 
    #     virtual_date: date,
    #     room_id: self.room_id
    #     meta_event: self
    #   )
    end
  end

  def date_chronology
    return true if self.start_date < self.end_date
    self.errors[:base] << "Los dias ingresados no son correctos."
    return false
  end

  def time_chronology
    return true if self.start_time < self.end_time
    self.errors[:base] << "Los horarios ingresados no son correctos."
    return false
  end

  def max_timelapse
    return true if ( self.start_date + 1.years =< self.end_date )
    self.errors[:base] << "Los horarios ingresados no son correctos."
    return false
  end

  def week_day
    return true if ( self.start_date.wday == self.end_date.wday )
    self.errors[:base] << "Los horarios ingresados no son correctos."
    return false
  end

  def set_virtual_dates
    require 'active_support/core_ext'
    self.start_date = self.start_dt.to_date
    self.end_date   = self.end_dt.to_date
  end

end
