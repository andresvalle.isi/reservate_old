class Room < ActiveRecord::Base
  has_many :reservations
  has_many :room_prizes
  validates :name, presence: true
  validates_uniqueness_of :name
end
