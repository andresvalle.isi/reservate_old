class Band < ActiveRecord::Base
  has_many :reservations
  validates :name, presence: true
  validates_uniqueness_of :name
end
