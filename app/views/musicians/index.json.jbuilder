json.array!(@musicians) do |musician|
  json.extract! musician, :id, :dni, :name, :lastname, :phone, :cellphone
  json.url musician_url(musician, format: :json)
end
