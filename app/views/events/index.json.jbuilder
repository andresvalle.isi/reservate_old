json.array!(@events) do |event|
  json.extract! event, :id, :event_id, :start, :end, :virtual_day, :status, :meta_event_id
  json.url event_url(event, format: :json)
end
