json.array!(@reservations) do |reservation|
  json.extract! reservation, :id, :room_id, :reservation_type_id, :musician_id, :band_id, :virtual_day, :since, :until
  json.url reservation_url(reservation, format: :json)
end
