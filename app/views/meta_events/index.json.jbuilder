json.array!(@meta_events) do |meta_event|
  json.extract! meta_event, :id, :meta_event_id, :start, :end, :virtual_day, :room_id, :musician_id
  json.url meta_event_url(meta_event, format: :json)
end
