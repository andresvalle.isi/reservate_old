json.array!(@room_prices) do |room_price|
  json.extract! room_price, :id, :room_id, :reservation_type_id, :amount, :since
  json.url room_price_url(room_price, format: :json)
end
