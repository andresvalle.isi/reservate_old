require 'test_helper'

class MetaEventsControllerTest < ActionController::TestCase
  setup do
    @meta_event = meta_events(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:meta_events)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create meta_event" do
    assert_difference('MetaEvent.count') do
      post :create, meta_event: { end: @meta_event.end, meta_event_id: @meta_event.meta_event_id, musician_id: @meta_event.musician_id, room_id: @meta_event.room_id, start: @meta_event.start, virtual_day: @meta_event.virtual_day }
    end

    assert_redirected_to meta_event_path(assigns(:meta_event))
  end

  test "should show meta_event" do
    get :show, id: @meta_event
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @meta_event
    assert_response :success
  end

  test "should update meta_event" do
    patch :update, id: @meta_event, meta_event: { end: @meta_event.end, meta_event_id: @meta_event.meta_event_id, musician_id: @meta_event.musician_id, room_id: @meta_event.room_id, start: @meta_event.start, virtual_day: @meta_event.virtual_day }
    assert_redirected_to meta_event_path(assigns(:meta_event))
  end

  test "should destroy meta_event" do
    assert_difference('MetaEvent.count', -1) do
      delete :destroy, id: @meta_event
    end

    assert_redirected_to meta_events_path
  end
end
