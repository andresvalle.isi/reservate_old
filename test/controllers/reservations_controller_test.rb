require 'test_helper'

class ReservationsControllerTest < ActionController::TestCase
  setup do
    @reservation = reservations(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:reservations)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create reservation" do
    assert_difference('Reservation.count') do
      post :create, reservation: { band_id: @reservation.band_id, musician_id: @reservation.musician_id, reservation_type_id: @reservation.reservation_type_id, room_id: @reservation.room_id, since: @reservation.since, until: @reservation.until, virtual_day: @reservation.virtual_day }
    end

    assert_redirected_to reservation_path(assigns(:reservation))
  end

  test "should show reservation" do
    get :show, id: @reservation
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @reservation
    assert_response :success
  end

  test "should update reservation" do
    patch :update, id: @reservation, reservation: { band_id: @reservation.band_id, musician_id: @reservation.musician_id, reservation_type_id: @reservation.reservation_type_id, room_id: @reservation.room_id, since: @reservation.since, until: @reservation.until, virtual_day: @reservation.virtual_day }
    assert_redirected_to reservation_path(assigns(:reservation))
  end

  test "should destroy reservation" do
    assert_difference('Reservation.count', -1) do
      delete :destroy, id: @reservation
    end

    assert_redirected_to reservations_path
  end
end
